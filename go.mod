module gitlab.com/cychiuae/go-pg-wrapper

go 1.14

require (
	github.com/go-pg/pg/v10 v10.6.2
	github.com/stretchr/testify v1.6.1
)
